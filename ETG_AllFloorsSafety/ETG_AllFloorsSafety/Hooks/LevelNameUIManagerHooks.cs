﻿using Dungeonator;
using System;

namespace ETG_AllFloorsSafety.Hooks
{
    class LevelNameUIManagerHooks
    {
        public void Hook_LevelNameUIManager_ShowLevelName(Action<LevelNameUIManager, Dungeon> orig, LevelNameUIManager self, Dungeon d)
        {
            if (GameManager.Instance.Dungeon.tileIndices.tilesetId == GlobalDungeonData.ValidTilesets.CASTLEGEON)
            {
                AllFloorsSafety.IsFireExtinguished = false;
            }

            orig(self, d);
        }

    }
}
