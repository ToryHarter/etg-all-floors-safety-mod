﻿using Dungeonator;
using System;
using System.Linq;

namespace ETG_AllFloorsSafety.Hooks
{
    class ProceduralFlowModifierDataHooks
    {
        // Copied from SpecialAPI's Discord message here: https://discord.com/channels/212584696410800130/227130661142659082/915234485421891584
        public bool Hook_ProceduralFlowModifierData_PrerequisitesMet(Func<ProceduralFlowModifierData, bool> orig, ProceduralFlowModifierData self)
        {
            bool result = orig(self);
            if (self.annotation == "Sell Creep (Nakatomi)" && !result)
            {
                for (int i = 0; i < self.prerequisites.Length; i++)
                {
                    if (!self.prerequisites[i].CheckConditionsFulfilled())
                    {
                        return false;
                    }
                }
                return !self.RequiresMasteryToken || !GameManager.HasInstance || !GameManager.Instance.PrimaryPlayer || (GameManager.Instance.PrimaryPlayer.MasteryTokensCollectedThisRun > 0 || GameManager.Instance.PrimaryPlayer.passiveItems.Any(x => x.PickupObjectId == PickupObjectDatabase.GetByName("Safety Round").PickupObjectId));
            }
            return result;
        }
    }
}
