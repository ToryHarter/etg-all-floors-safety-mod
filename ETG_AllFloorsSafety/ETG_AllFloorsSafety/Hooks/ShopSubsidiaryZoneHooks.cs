﻿using Dungeonator;
using System.Collections.Generic;
using UnityEngine;

namespace ETG_AllFloorsSafety.Hooks
{
    class ShopSubsidiaryZoneHooks
    {
        public delegate void CustomAction<T1, T2, T3, T4, T5>(T1 p1, T2 p2, T3 p3, T4 p4, T5 p5);

        public void Hook_ShopSubsidiaryZone_HandleSetup(CustomAction<ShopSubsidiaryZone, BaseShopController, RoomHandler, List<GameObject>, List<ShopItemController>> orig, ShopSubsidiaryZone self, BaseShopController controller, RoomHandler room, List<GameObject> shopItemObjects, List<ShopItemController> shopItemControllers)
        {
            int count = shopItemObjects.Count;
            for (int i = 0; i < self.spawnPositions.Length; i++)
            {
                GameObject item;
                if (self.IsShopRoundTable && i == 0 && GameManager.Instance.Dungeon.tileIndices.tilesetId == GlobalDungeonData.ValidTilesets.GUNGEON)
                {
                    item = PickupObjectDatabase.GetById(GlobalItemIds.CathedralCrest).gameObject;
                }
                else if (self.IsShopRoundTable && (i == 0 || i == 1) && GameManager.Instance.Dungeon.tileIndices.tilesetId == GlobalDungeonData.ValidTilesets.MINEGEON)
                {
                    item = PickupObjectDatabase.GetById(GlobalItemIds.Blank).gameObject;
                }
                else
                {
                    item = self.shopItems.SelectByWeightWithoutDuplicatesFullPrereqs(shopItemObjects, true, false);
                }

                shopItemObjects.Add(item);
            }
            bool flag = false;
            for (int j = 0; j < self.spawnPositions.Length; j++)
            {
                if (!(shopItemObjects[count + j] == null))
                {
                    flag = true;
                    Transform transform = self.spawnPositions[j];
                    PickupObject component = shopItemObjects[count + j].GetComponent<PickupObject>();
                    if (!(component == null))
                    {
                        GameObject gameObject = new GameObject("Shop item " + j.ToString());
                        Transform transform2 = gameObject.transform;
                        transform2.parent = transform;
                        transform2.localPosition = Vector3.zero;
                        EncounterTrackable component2 = component.GetComponent<EncounterTrackable>();
                        if (component2 != null)
                        {
                            GameManager.Instance.ExtantShopTrackableGuids.Add(component2.EncounterGuid);
                        }
                        ShopItemController shopItemController = gameObject.AddComponent<ShopItemController>();
                        shopItemController.PrecludeAllDiscounts = self.PrecludeAllDiscounts;
                        if (transform.name.Contains("SIDE") || transform.name.Contains("EAST"))
                        {
                            shopItemController.itemFacing = DungeonData.Direction.EAST;
                        }
                        else if (transform.name.Contains("WEST"))
                        {
                            shopItemController.itemFacing = DungeonData.Direction.WEST;
                        }
                        else if (transform.name.Contains("NORTH"))
                        {
                            shopItemController.itemFacing = DungeonData.Direction.NORTH;
                        }
                        if (!room.IsRegistered(shopItemController))
                        {
                            room.RegisterInteractable(shopItemController);
                        }
                        shopItemController.Initialize(component, controller);
                        if (shopItemController.item.PickupObjectId == GlobalItemIds.CathedralCrest)
                        {
                            shopItemController.OverridePrice = 0;
                            shopItemController.CurrentPrice = 0;
                            shopItemController.item.PersistsOnPurchase = true;

                        }
                        shopItemControllers.Add(shopItemController);
                    }
                }
            }
            if (!flag)
            {
                SpeculativeRigidbody[] componentsInChildren = ((Component)self).GetComponentsInChildren<SpeculativeRigidbody>();
                for (int k = 0; k < componentsInChildren.Length; k++)
                {
                    componentsInChildren[k].enabled = false;
                }
                ((Component)self).gameObject.SetActive(false);
            }
        }
    }
}
