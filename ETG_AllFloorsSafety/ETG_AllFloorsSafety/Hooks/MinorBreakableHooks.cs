﻿using System;
using UnityEngine;

namespace ETG_AllFloorsSafety.Hooks
{
    class MinorBreakableHooks
    {
        public void Hook_MinorBreakable_Break_NoParams(Action<MinorBreakable> orig, MinorBreakable self)
        {
            if (!IsWaterDrum(self))
            {
                orig(self);
            }
            else if (AllFloorsSafety.IsFireExtinguished && IsWaterDrum(self))
            {
                AllFloorsSafety.IsFireExtinguished = false;
                orig(self);
            }
        }

        public void Hook_MinorBreakable_Break_WithParams(Action<MinorBreakable, Vector2> orig, MinorBreakable self, Vector2 direction)
        {
            if (!IsWaterDrum(self))
            {
                orig(self, direction);
            }
            else if (AllFloorsSafety.IsFireExtinguished && IsWaterDrum(self))
            {
                AllFloorsSafety.IsFireExtinguished = false;
                orig(self, direction);
            }
        }

        private bool IsWaterDrum(MinorBreakable breakable)
        {
            return GameManager.Instance.Dungeon.tileIndices.tilesetId == GlobalDungeonData.ValidTilesets.CASTLEGEON
                    && breakable.goopType != null
                    && !breakable.goopType.CanBeIgnited;
        }
    }
}
