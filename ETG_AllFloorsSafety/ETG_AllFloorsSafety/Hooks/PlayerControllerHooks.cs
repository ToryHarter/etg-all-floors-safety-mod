﻿using System;


namespace ETG_AllFloorsSafety.Hooks
{
    class PlayerControllerHooks
    {
        private void Hook_PlayerController_InitializeInventory(Action<PlayerController> orig, PlayerController self)
        {
            orig(self);

            self.AcquirePassiveItemPrefabDirectly(PickupObjectDatabase.GetByName("Safety Round") as PassiveItem);
        }
    }
}
