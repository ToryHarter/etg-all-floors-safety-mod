﻿using System;
using System.Reflection;
using UnityEngine;

namespace ETG_AllFloorsSafety.Hooks
{
    class ShopItemControllerHooks
    {
        private bool IsCrestAndPlayerHasCrestInGungeon(ShopItemController self, PlayerController player)
        {
            return GameManager.Instance.Dungeon.tileIndices.tilesetId == GlobalDungeonData.ValidTilesets.GUNGEON
                    && self.item.PickupObjectId == GlobalItemIds.CathedralCrest
                    && player.healthHaver.HasCrest;
        }

        public void Hook_ShopItemController_Interact(Action<ShopItemController, PlayerController> orig, ShopItemController self, PlayerController player)
        {
            if (IsCrestAndPlayerHasCrestInGungeon(self, player)) return;

            FieldInfo m_baseParentShop = typeof(ShopItemController).GetField("m_baseParentShop", BindingFlags.NonPublic | BindingFlags.Instance);
            BaseShopController _baseParentShop = (BaseShopController)m_baseParentShop.GetValue(self);
            //_baseParentShop = (BaseShopController)(typeof(ShopItemController).GetField("m_baseParentShop", BindingFlags.NonPublic | BindingFlags.Instance)).GetValue(self);

            FieldInfo m_parentShop = typeof(ShopItemController).GetField("m_parentShop", BindingFlags.NonPublic | BindingFlags.Instance);
            ShopController _parentShop = (ShopController)m_parentShop.GetValue(self);
            //_parentShop = (ShopController)(typeof(ShopItemController).GetField("m_parentShop", BindingFlags.NonPublic | BindingFlags.Instance)).GetValue(self);

            FieldInfo m_pickedUp = typeof(ShopItemController).GetField("pickedUp", BindingFlags.NonPublic | BindingFlags.Instance);
            bool _pickedUp = (bool)m_pickedUp.GetValue(self);
            //_pickedUp = (bool)(typeof(ShopItemController).GetField("pickedUp", BindingFlags.NonPublic | BindingFlags.Instance)).GetValue(self);

            if (self.item && self.item is HealthPickup)
            {
                if ((self.item as HealthPickup).healAmount > 0f && (self.item as HealthPickup).armorAmount <= 0 && player.healthHaver.GetCurrentHealthPercentage() >= 1f)
                {
                    return;
                }
            }
            else if (self.item && self.item is AmmoPickup && (player.CurrentGun == null || player.CurrentGun.ammo == player.CurrentGun.AdjustedMaxAmmo || !player.CurrentGun.CanGainAmmo || player.CurrentGun.InfiniteAmmo))
            {
                GameUIRoot.Instance.InformNeedsReload(player, new Vector3(player.specRigidbody.UnitCenter.x - player.transform.position.x, 1.25f, 0f), 1f, "#RELOAD_FULL");
                return;
            }
            self.LastInteractingPlayer = player;
            if (self.CurrencyType == ShopItemController.ShopCurrencyType.COINS || self.CurrencyType == ShopItemController.ShopCurrencyType.BLANKS || self.CurrencyType == ShopItemController.ShopCurrencyType.KEYS)
            {
                bool flag = false;
                bool flag2 = true;

                _baseParentShop = (BaseShopController)(typeof(ShopItemController).GetField("m_baseParentShop", BindingFlags.NonPublic | BindingFlags.Instance)).GetValue(self);
                var shouldSteal = GameManager.Instance.CurrentLevelOverrideState != GameManager.LevelOverrideState.FOYER && (_baseParentShop.IsCapableOfBeingStolenFrom || player.IsCapableOfStealing);

                if (shouldSteal)
                {
                    _baseParentShop = (BaseShopController)(typeof(ShopItemController).GetField("m_baseParentShop", BindingFlags.NonPublic | BindingFlags.Instance)).GetValue(self);
                    flag = _baseParentShop.AttemptToSteal();
                    m_baseParentShop.SetValue(self, _baseParentShop);
                    flag2 = false;
                    if (!flag)
                    {
                        player.DidUnstealthyAction();
                        _baseParentShop = (BaseShopController)(typeof(ShopItemController).GetField("m_baseParentShop", BindingFlags.NonPublic | BindingFlags.Instance)).GetValue(self);
                        _baseParentShop.NotifyStealFailed();
                        m_baseParentShop.SetValue(self, _baseParentShop);
                        return;
                    }
                }
                if (flag2)
                {
                    bool flag3 = false;
                    if (self.CurrencyType == ShopItemController.ShopCurrencyType.COINS || self.CurrencyType == ShopItemController.ShopCurrencyType.BLANKS)
                    {
                        flag3 = (player.carriedConsumables.Currency >= self.ModifiedPrice) || (self.ModifiedPrice == 0);
                    }
                    else if (self.CurrencyType == ShopItemController.ShopCurrencyType.KEYS)
                    {
                        flag3 = (player.carriedConsumables.KeyBullets >= self.ModifiedPrice);
                    }
                    if (self.IsResourcefulRatKey)
                    {
                        player.carriedConsumables.Currency -= self.ModifiedPrice;
                        GameStatsManager.Instance.RegisterStatChange(TrackedStats.AMOUNT_PAID_FOR_RAT_KEY, (float)self.ModifiedPrice);
                        flag2 = false;
                    }
                    else if (!flag3)
                    {
                        AkSoundEngine.PostEvent("Play_OBJ_purchase_unable_01", self.gameObject);
                        _parentShop = (ShopController)(typeof(ShopItemController).GetField("m_parentShop", BindingFlags.NonPublic | BindingFlags.Instance)).GetValue(self);
                        if (_parentShop != null)
                        {
                            _parentShop = (ShopController)(typeof(ShopItemController).GetField("m_parentShop", BindingFlags.NonPublic | BindingFlags.Instance)).GetValue(self);
                            _parentShop.NotifyFailedPurchase(self);
                            m_parentShop.SetValue(self, _parentShop);
                        }
                        _baseParentShop = (BaseShopController)(typeof(ShopItemController).GetField("m_baseParentShop", BindingFlags.NonPublic | BindingFlags.Instance)).GetValue(self);
                        if (_baseParentShop != null)
                        {
                            _baseParentShop = (BaseShopController)(typeof(ShopItemController).GetField("m_baseParentShop", BindingFlags.NonPublic | BindingFlags.Instance)).GetValue(self);
                            _baseParentShop.NotifyFailedPurchase(self);
                            m_baseParentShop.SetValue(self, _baseParentShop);
                        }
                        return;
                    }
                }
                _pickedUp = (bool)(typeof(ShopItemController).GetField("pickedUp", BindingFlags.NonPublic | BindingFlags.Instance)).GetValue(self);
                if (!_pickedUp)
                {
                    _pickedUp = (bool)(typeof(ShopItemController).GetField("pickedUp", BindingFlags.NonPublic | BindingFlags.Instance)).GetValue(self);
                    _pickedUp = !self.item.PersistsOnPurchase;
                    m_pickedUp.SetValue(self, _pickedUp);

                    LootEngine.GivePrefabToPlayer(self.item.gameObject, player);
                    if (flag2)
                    {
                        if (self.CurrencyType == ShopItemController.ShopCurrencyType.COINS || self.CurrencyType == ShopItemController.ShopCurrencyType.BLANKS)
                        {
                            player.carriedConsumables.Currency -= self.ModifiedPrice;
                        }
                        else if (self.CurrencyType == ShopItemController.ShopCurrencyType.KEYS)
                        {
                            player.carriedConsumables.KeyBullets -= self.ModifiedPrice;
                        }
                    }
                    _parentShop = (ShopController)(typeof(ShopItemController).GetField("m_parentShop", BindingFlags.NonPublic | BindingFlags.Instance)).GetValue(self);
                    if (_parentShop != null)
                    {
                        _parentShop = (ShopController)(typeof(ShopItemController).GetField("m_parentShop", BindingFlags.NonPublic | BindingFlags.Instance)).GetValue(self);
                        _parentShop.PurchaseItem(self, !flag, true);
                        m_parentShop.SetValue(self, _parentShop);
                    }
                    _baseParentShop = (BaseShopController)(typeof(ShopItemController).GetField("m_baseParentShop", BindingFlags.NonPublic | BindingFlags.Instance)).GetValue(self);
                    if (_baseParentShop != null)
                    {
                        _baseParentShop = (BaseShopController)(typeof(ShopItemController).GetField("m_baseParentShop", BindingFlags.NonPublic | BindingFlags.Instance)).GetValue(self);
                        _baseParentShop.PurchaseItem(self, !flag, true);
                        m_baseParentShop.SetValue(self, _baseParentShop);
                    }
                    if (flag)
                    {
                        StatModifier statModifier = new StatModifier();
                        statModifier.statToBoost = PlayerStats.StatType.Curse;
                        statModifier.amount = 1f;
                        statModifier.modifyType = StatModifier.ModifyMethod.ADDITIVE;
                        player.ownerlessStatModifiers.Add(statModifier);
                        player.stats.RecalculateStats(player, false, false);
                        player.HandleItemStolen(self);

                        _baseParentShop = (BaseShopController)(typeof(ShopItemController).GetField("m_baseParentShop", BindingFlags.NonPublic | BindingFlags.Instance)).GetValue(self);
                        _baseParentShop.NotifyStealSucceeded();
                        m_baseParentShop.SetValue(self, _baseParentShop);

                        player.IsThief = true;
                        GameStatsManager.Instance.RegisterStatChange(TrackedStats.MERCHANT_ITEMS_STOLEN, 1f);
                        if (self.SetsFlagOnSteal)
                        {
                            GameStatsManager.Instance.SetFlag(self.FlagToSetOnSteal, true);
                        }
                    }
                    else
                    {
                        if (self.CurrencyType == ShopItemController.ShopCurrencyType.BLANKS)
                        {
                            player.Blanks++;
                        }
                        player.HandleItemPurchased(self);
                    }
                    if (!self.item.PersistsOnPurchase)
                    {
                        GameUIRoot.Instance.DeregisterDefaultLabel(((BraveBehaviour)self).transform);
                    }
                    AkSoundEngine.PostEvent("Play_OBJ_item_purchase_01", ((Component)self).gameObject);
                }
            }
            else if (self.CurrencyType == ShopItemController.ShopCurrencyType.META_CURRENCY)
            {
                int num2 = Mathf.RoundToInt(GameStatsManager.Instance.GetPlayerStatValue(TrackedStats.META_CURRENCY));
                if (num2 < self.ModifiedPrice)
                {
                    AkSoundEngine.PostEvent("Play_OBJ_purchase_unable_01", ((Component)self).gameObject);
                    _parentShop = (ShopController)(typeof(ShopItemController).GetField("m_parentShop", BindingFlags.NonPublic | BindingFlags.Instance)).GetValue(self);
                    if (_parentShop != null)
                    {
                        _parentShop = (ShopController)(typeof(ShopItemController).GetField("m_parentShop", BindingFlags.NonPublic | BindingFlags.Instance)).GetValue(self);
                        _parentShop.NotifyFailedPurchase(self);
                        m_parentShop.SetValue(self, _parentShop);
                    }
                    _baseParentShop = (BaseShopController)(typeof(ShopItemController).GetField("m_baseParentShop", BindingFlags.NonPublic | BindingFlags.Instance)).GetValue(self);
                    if (_baseParentShop != null)
                    {
                        _baseParentShop = (BaseShopController)(typeof(ShopItemController).GetField("m_baseParentShop", BindingFlags.NonPublic | BindingFlags.Instance)).GetValue(self);
                        _baseParentShop.NotifyFailedPurchase(self);
                        m_baseParentShop.SetValue(self, _baseParentShop);
                    }
                    return;
                }
                _pickedUp = (bool)(typeof(ShopItemController).GetField("pickedUp", BindingFlags.NonPublic | BindingFlags.Instance)).GetValue(self);
                if (!_pickedUp)
                {
                    _pickedUp = (bool)(typeof(ShopItemController).GetField("pickedUp", BindingFlags.NonPublic | BindingFlags.Instance)).GetValue(self);
                    _pickedUp = !self.item.PersistsOnPurchase;
                    m_pickedUp.SetValue(self, _pickedUp);

                    GameStatsManager.Instance.ClearStatValueGlobal(TrackedStats.META_CURRENCY);
                    GameStatsManager.Instance.SetStat(TrackedStats.META_CURRENCY, (float)(num2 - self.ModifiedPrice));
                    GameStatsManager.Instance.RegisterStatChange(TrackedStats.META_CURRENCY_SPENT_AT_META_SHOP, (float)self.ModifiedPrice);
                    LootEngine.GivePrefabToPlayer(self.item.gameObject, player);

                    _parentShop = (ShopController)(typeof(ShopItemController).GetField("m_parentShop", BindingFlags.NonPublic | BindingFlags.Instance)).GetValue(self);
                    if (_parentShop != null)
                    {
                        _parentShop = (ShopController)(typeof(ShopItemController).GetField("m_parentShop", BindingFlags.NonPublic | BindingFlags.Instance)).GetValue(self);
                        _parentShop.PurchaseItem(self, true, true);
                        m_parentShop.SetValue(self, _parentShop);
                    }
                    _baseParentShop = (BaseShopController)(typeof(ShopItemController).GetField("m_baseParentShop", BindingFlags.NonPublic | BindingFlags.Instance)).GetValue(self);
                    if (_baseParentShop != null)
                    {
                        _baseParentShop = (BaseShopController)(typeof(ShopItemController).GetField("m_baseParentShop", BindingFlags.NonPublic | BindingFlags.Instance)).GetValue(self);
                        _baseParentShop.PurchaseItem(self, true, true);
                        m_baseParentShop.SetValue(self, _baseParentShop);
                    }
                    player.HandleItemPurchased(self);
                    if (!self.item.PersistsOnPurchase)
                    {
                        GameUIRoot.Instance.DeregisterDefaultLabel(((BraveBehaviour)self).transform);
                    }
                    AkSoundEngine.PostEvent("Play_OBJ_item_purchase_01", ((Component)self).gameObject);
                }
            }
        }
    }
}
