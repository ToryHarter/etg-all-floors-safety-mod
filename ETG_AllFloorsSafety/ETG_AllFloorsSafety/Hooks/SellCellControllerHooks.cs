﻿using Dungeonator;
using System;
using System.Collections;
using System.Reflection;
using UnityEngine;

namespace ETG_AllFloorsSafety.Hooks
{
    class SellCellControllerHooks
    {
        private IEnumerator Hook_SellCellController_HandleSoldItem(Func<SellCellController, PickupObject, IEnumerator> orig, SellCellController self, PickupObject targetItem)
        {
            FieldInfo m_currentlySellingAnItem = typeof(SellCellController).GetField("m_currentlySellingAnItem", BindingFlags.NonPublic | BindingFlags.Instance);
            bool _currentlySellingAnItem = (bool)m_currentlySellingAnItem.GetValue(self);
            //_currentlySellingAnItem = (bool)(typeof(SellCellController).GetField("m_currentlySellingAnItem", BindingFlags.NonPublic | BindingFlags.Instance)).GetValue(self);

            FieldInfo m_thingsSold = typeof(SellCellController).GetField("m_thingsSold", BindingFlags.NonPublic | BindingFlags.Instance);
            int _thingsSold = (int)m_thingsSold.GetValue(self);
            //_thingsSold = (int)(typeof(SellCellController).GetField("m_thingsSold", BindingFlags.NonPublic | BindingFlags.Instance)).GetValue(self);

            FieldInfo m_masteryRoundsSold = typeof(SellCellController).GetField("m_masteryRoundsSold", BindingFlags.NonPublic | BindingFlags.Instance);
            int _masteryRoundsSold = (int)m_masteryRoundsSold.GetValue(self);
            //_masteryRoundsSold = (int)(typeof(SellCellController).GetField("m_masteryRoundsSold", BindingFlags.NonPublic | BindingFlags.Instance)).GetValue(self);

            targetItem.IsBeingSold = true;
            while ((bool)(typeof(SellCellController).GetField("m_currentlySellingAnItem", BindingFlags.NonPublic | BindingFlags.Instance)).GetValue(self))
            {
                yield return null;
            }
            if (!targetItem)
            {
                yield break;
            }
            if (!targetItem.sprite || !self.specRigidbody.ContainsPoint(targetItem.sprite.WorldCenter, 2147483647, true))
            {
                yield break;
            }
            _currentlySellingAnItem = (bool)(typeof(SellCellController).GetField("m_currentlySellingAnItem", BindingFlags.NonPublic | BindingFlags.Instance)).GetValue(self);
            _currentlySellingAnItem = true;
            m_currentlySellingAnItem.SetValue(self, _currentlySellingAnItem);
            IPlayerInteractable ixable = null;
            if (targetItem is PassiveItem)
            {
                PassiveItem passiveItem = targetItem as PassiveItem;
                passiveItem.GetRidOfMinimapIcon();
                ixable = (targetItem as PassiveItem);
            }
            else if (targetItem is Gun)
            {
                Gun gun = targetItem as Gun;
                gun.GetRidOfMinimapIcon();
                ixable = (targetItem as Gun);
            }
            else if (targetItem is PlayerItem)
            {
                PlayerItem playerItem = targetItem as PlayerItem;
                playerItem.GetRidOfMinimapIcon();
                ixable = (targetItem as PlayerItem);
            }
            if (ixable != null)
            {
                RoomHandler.unassignedInteractableObjects.Remove(ixable);
                GameManager.Instance.PrimaryPlayer.RemoveBrokenInteractable(ixable);
            }
            float elapsed = 0f;
            float duration = 0.5f;
            Vector3 startPos = targetItem.transform.position;
            Vector3 finalOffset = Vector3.zero;
            tk2dBaseSprite targetSprite = targetItem.GetComponentInChildren<tk2dBaseSprite>();
            if (targetSprite)
            {
                finalOffset = targetSprite.GetBounds().extents;
            }
            while (elapsed < duration)
            {
                elapsed += BraveTime.DeltaTime;
                if (!targetItem || !targetItem.transform)
                {
                    _currentlySellingAnItem = (bool)(typeof(SellCellController).GetField("m_currentlySellingAnItem", BindingFlags.NonPublic | BindingFlags.Instance)).GetValue(self);
                    _currentlySellingAnItem = false;
                    m_currentlySellingAnItem.SetValue(self, _currentlySellingAnItem);

                    yield break;
                }
                targetItem.transform.localScale = Vector3.Lerp(Vector3.one, new Vector3(0.01f, 0.01f, 1f), elapsed / duration);
                targetItem.transform.position = Vector3.Lerp(startPos, startPos + new Vector3(finalOffset.x, 0f, 0f), elapsed / duration);
                yield return null;
            }
            if (!targetItem || !targetItem.transform)
            {
                _currentlySellingAnItem = (bool)(typeof(SellCellController).GetField("m_currentlySellingAnItem", BindingFlags.NonPublic | BindingFlags.Instance)).GetValue(self);
                _currentlySellingAnItem = false;
                m_currentlySellingAnItem.SetValue(self, _currentlySellingAnItem);

                yield break;
            }
            self.SellPitDweller.SendPlaymakerEvent("playerSoldSomething");
            int sellPrice = Mathf.Clamp(Mathf.CeilToInt((float)targetItem.PurchasePrice * self.SellValueModifier), 0, 200);
            if (targetItem.quality == PickupObject.ItemQuality.SPECIAL || targetItem.quality == PickupObject.ItemQuality.EXCLUDED)
            {
                sellPrice = 3;
            }
            LootEngine.SpawnCurrency(targetItem.sprite.WorldCenter, sellPrice, false);

            _thingsSold = (int)(typeof(SellCellController).GetField("m_thingsSold", BindingFlags.NonPublic | BindingFlags.Instance)).GetValue(self);
            _thingsSold++;
            m_thingsSold.SetValue(self, _thingsSold);

            if (targetItem.PickupObjectId == GlobalItemIds.MasteryToken_Castle || targetItem.PickupObjectId == GlobalItemIds.MasteryToken_Catacombs || targetItem.PickupObjectId == GlobalItemIds.MasteryToken_Gungeon || targetItem.PickupObjectId == GlobalItemIds.MasteryToken_Forge || targetItem.PickupObjectId == GlobalItemIds.MasteryToken_Mines || targetItem.PickupObjectId == PickupObjectDatabase.GetByName("Safety Round").PickupObjectId)
            {
                _masteryRoundsSold = (int)(typeof(SellCellController).GetField("m_masteryRoundsSold", BindingFlags.NonPublic | BindingFlags.Instance)).GetValue(self);
                _masteryRoundsSold++;
                m_masteryRoundsSold.SetValue(self, _masteryRoundsSold);
            }
            if (targetItem is Gun && targetItem.GetComponentInParent<DebrisObject>())
            {
                UnityEngine.Object.Destroy(targetItem.transform.parent.gameObject);
            }
            else
            {
                UnityEngine.Object.Destroy(targetItem.gameObject);
            }

            _thingsSold = (int)(typeof(SellCellController).GetField("m_thingsSold", BindingFlags.NonPublic | BindingFlags.Instance)).GetValue(self);
            _masteryRoundsSold = (int)(typeof(SellCellController).GetField("m_masteryRoundsSold", BindingFlags.NonPublic | BindingFlags.Instance)).GetValue(self);
            if (_thingsSold >= 3 && _masteryRoundsSold > 0)
            {
                self.StartCoroutine(HandleSellPitOpening(self));
            }
            _currentlySellingAnItem = (bool)(typeof(SellCellController).GetField("m_currentlySellingAnItem", BindingFlags.NonPublic | BindingFlags.Instance)).GetValue(self);
            _currentlySellingAnItem = false;
            m_currentlySellingAnItem.SetValue(self, _currentlySellingAnItem);

            yield break;
        }

        private IEnumerator HandleSellPitOpening(SellCellController self)
        {
            FieldInfo m_isExploded = typeof(SellCellController).GetField("m_isExploded", BindingFlags.NonPublic | BindingFlags.Instance);
            bool _isExploded = (bool)m_isExploded.GetValue(self);
            //_isExploded = (bool)(typeof(SellCellController).GetField("m_isExploded", BindingFlags.NonPublic | BindingFlags.Instance)).GetValue(self);

            if (GameManager.Instance.Dungeon.tileIndices.tilesetId != GlobalDungeonData.ValidTilesets.CATACOMBGEON)
            {
                yield break;
            }

            _isExploded = (bool)(typeof(SellCellController).GetField("m_isExploded", BindingFlags.NonPublic | BindingFlags.Instance)).GetValue(self);
            _isExploded = true;
            m_isExploded.SetValue(self, _isExploded);

            self.SellPitDweller.PreventInteraction = true;
            self.SellPitDweller.PreventCoopInteraction = true;
            self.SellPitDweller.playerApproachRadius = -1f;
            yield return new WaitForSeconds(3f);
            UnityEngine.Object.Instantiate<GameObject>(self.SellExplosionVFX, self.transform.position, Quaternion.identity);
            float elapsed = 0f;
            while (elapsed < 0.25f)
            {
                elapsed += BraveTime.DeltaTime;
                yield return null;
            }
            self.CellTopSprite.SetSprite(self.ExplodedSellSpriteName);
            for (int i = 1; i < self.GetWidth(); i++)
            {
                for (int j = 0; j < self.GetHeight(); j++)
                {
                    IntVector2 intVector = self.transform.position.IntXY(VectorConversions.Round) + new IntVector2(i, j);
                    if (GameManager.Instance.Dungeon.data.CheckInBoundsAndValid(intVector))
                    {
                        CellData cellData = GameManager.Instance.Dungeon.data[intVector];
                        cellData.fallingPrevented = false;
                    }
                }
            }
            yield break;
        }
    }
}
