﻿using System;
using System.Reflection;
using UnityEngine;

namespace ETG_AllFloorsSafety.Hooks
{
    class PlayerConsumablesHooks
    {
        public void Hook_PlayerConsumables_Currency_Set(Action<PlayerConsumables, int> orig, PlayerConsumables self, int value)
        {
            FieldInfo m_currency = typeof(PlayerConsumables).GetField("m_currency", BindingFlags.NonPublic | BindingFlags.Instance);
            int _currency = (int)m_currency.GetValue(self);
            //_currency = (int)(typeof(PlayerConsumables).GetField("m_currency", BindingFlags.NonPublic)).GetValue(self);

            int num = value;
            _currency = (int)(typeof(PlayerConsumables).GetField("m_currency", BindingFlags.NonPublic | BindingFlags.Instance)).GetValue(self);
            if (num > _currency && GameStatsManager.HasInstance)
            {
                _currency = (int)(typeof(PlayerConsumables).GetField("m_currency", BindingFlags.NonPublic | BindingFlags.Instance)).GetValue(self);
                GameStatsManager.Instance.RegisterStatChange(TrackedStats.TOTAL_MONEY_COLLECTED, (float)(num - _currency));
            }
            if (num >= 300 && GameManager.HasInstance && GameManager.Instance.platformInterface != null)
            {
                float realtimeSinceStartup = Time.realtimeSinceStartup;
                if (realtimeSinceStartup > PlatformInterface.LastManyCoinsUnlockTime + 5f || realtimeSinceStartup < PlatformInterface.LastManyCoinsUnlockTime)
                {
                    GameManager.Instance.platformInterface.AchievementUnlock(Achievement.HAVE_MANY_COINS, 0);
                    PlatformInterface.LastManyCoinsUnlockTime = realtimeSinceStartup;
                }
            }
            _currency = (int)(typeof(PlayerConsumables).GetField("m_currency", BindingFlags.NonPublic | BindingFlags.Instance)).GetValue(self);
            _currency = num;
            m_currency.SetValue(self, _currency);

            if (GameUIRoot.HasInstance)
            {
                GameUIRoot.Instance.UpdatePlayerConsumables(self);
            }
        }
    }
}
