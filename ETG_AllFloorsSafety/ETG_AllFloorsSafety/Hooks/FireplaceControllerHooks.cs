﻿using System;


namespace ETG_AllFloorsSafety.Hooks
{
    class FireplaceControllerHooks
    {
        private void Hook_FireplaceController_OnFireExtinguished(Action<FireplaceController> orig, FireplaceController self)
        {
            orig(self);

            AllFloorsSafety.IsFireExtinguished = true;
        }
    }
}
