﻿using ItemAPI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace ETG_AllFloorsSafety
{
    public class SafetyRound : PassiveItem
    {
        public static void Init()
        {
            string itemName = "Safety Round";
            string resourceName = "ETG_AllFloorsSafety/Resources/SafetyRound.png";

            GameObject obj = new GameObject(itemName);

            var item = obj.AddComponent<PassiveItem>();

            item.RemovePickupFromLootTables();

            ItemBuilder.AddSpriteToObject(itemName, resourceName, obj);

            string shortDesc = "Safety First";
            string longDesc = "Don't have a Master Round for the Sell Creep? Use the Safety Round!";

            ItemBuilder.SetupItem(item, shortDesc, longDesc, "ETG_AllFloorsSafety");

            item.quality = PickupObject.ItemQuality.SPECIAL;

            item.RemovePickupFromLootTables();
        }
    }
}
