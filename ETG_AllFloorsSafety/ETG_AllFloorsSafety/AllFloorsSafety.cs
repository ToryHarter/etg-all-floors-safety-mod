﻿using ETG_AllFloorsSafety.Hooks;
using MonoMod.RuntimeDetour;
using MonoMod.Utils;
using System;
using System.Reflection;
using UnityEngine;
using ItemAPI;
using Dungeonator;
using System.Collections.Generic;

namespace ETG_AllFloorsSafety
{
    public class AllFloorsSafety : ETGModule
    {
        public override void Exit() {}
        public override void Init() {}

        public static bool IsFireExtinguished;

        public override void Start()
        {
            Console.WriteLine($"ETG_AllFloorsSafety Start()");

            this.SetupHooks();
            this.SetupItems();

            IsFireExtinguished = false;
        }

        private void SetupItems()
        {
            Console.WriteLine($"ETG_AllFloorsSafety SetupItems()");

            FakePrefabHooks.Init();
            ItemBuilder.Init();

            SafetyRound.Init();
        }

        private void SetupHooks()
        {
            Console.WriteLine($"ETG_AllFloorsSafety SetupHooks()");

            new Hook(
                typeof(PlayerConsumables).GetProperty("Currency", BindingFlags.Public | BindingFlags.Instance).GetSetMethod(),
                typeof(PlayerConsumablesHooks).GetMethod("Hook_PlayerConsumables_Currency_Set", BindingFlags.Public | BindingFlags.Instance),
                this);

            new Hook(
                typeof(MinorBreakable).GetMethod("Break", Type.EmptyTypes),
                typeof(MinorBreakableHooks).GetMethod("Hook_MinorBreakable_Break_NoParams", BindingFlags.Public | BindingFlags.Instance),
                this);

            new Hook(
                typeof(MinorBreakable).GetMethod("Break", new Type[] { typeof(Vector2) }),
                typeof(MinorBreakableHooks).GetMethod("Hook_MinorBreakable_Break_WithParams", BindingFlags.Public | BindingFlags.Instance),
                this);

            new Hook(
                typeof(FireplaceController).GetMethod("OnFireExtinguished", BindingFlags.NonPublic | BindingFlags.Instance),
                typeof(FireplaceControllerHooks).GetMethod("Hook_FireplaceController_OnFireExtinguished", BindingFlags.NonPublic | BindingFlags.Instance),
                this);

            new Hook(
                typeof(LevelNameUIManager).GetMethod("ShowLevelName", BindingFlags.Public | BindingFlags.Instance),
                typeof(LevelNameUIManagerHooks).GetMethod("Hook_LevelNameUIManager_ShowLevelName", BindingFlags.Public | BindingFlags.Instance),
                this);


            new Hook(
                typeof(ShopItemController).GetMethod("Interact", BindingFlags.Public | BindingFlags.Instance),
                typeof(ShopItemControllerHooks).GetMethod("Hook_ShopItemController_Interact", BindingFlags.Public | BindingFlags.Instance),
                this);

            new Hook(
                typeof(PlayerController).GetMethod("InitializeInventory", BindingFlags.NonPublic | BindingFlags.Instance),
                typeof(PlayerControllerHooks).GetMethod("Hook_PlayerController_InitializeInventory", BindingFlags.NonPublic | BindingFlags.Instance),
                this);

            new Hook(
                typeof(ProceduralFlowModifierData).GetProperty("PrerequisitesMet", BindingFlags.Public | BindingFlags.Instance).GetGetMethod(),
                typeof(ProceduralFlowModifierDataHooks).GetMethod("Hook_ProceduralFlowModifierData_PrerequisitesMet", BindingFlags.Public | BindingFlags.Instance),
                this);

            new Hook(
                typeof(SellCellController).GetMethod("HandleSoldItem", BindingFlags.NonPublic | BindingFlags.Instance),
                typeof(SellCellControllerHooks).GetMethod("Hook_SellCellController_HandleSoldItem", BindingFlags.NonPublic | BindingFlags.Instance),
                this);

            new Hook(
                typeof(ShopSubsidiaryZone).GetMethod("HandleSetup", new Type[] { typeof(BaseShopController), typeof(RoomHandler), typeof(List<GameObject>), typeof(List<ShopItemController>) }),
                typeof(ShopSubsidiaryZoneHooks).GetMethod("Hook_ShopSubsidiaryZone_HandleSetup", BindingFlags.Public | BindingFlags.Instance),
                this);
        }
    }
}
